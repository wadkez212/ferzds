<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FERZDS</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-grid.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/about_us.css">
</head>
<body>

	<?php $this_page = 'about' ?>

	@include('/partitials/header')
	
	@include('/partitials/top')

	@include('/partitials/about_part')

	@include('/partitials/reviews_part')

	@include('/partitials/staf_team_carousel')

	@include('/partitials/footer')

	

	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
	<script src="../js/polyfills.js"></script>
	<script src="../js/main.js" type="module"></script>
</body>
</html>