	<section class="why__we">
		<div class="container">
			<div class="why__we-inner">
				<div class="why__we-title title">

				</div>
				<div class="why__we-title-media title">
					ЭТАПЫ РАЗРАБОТКИ
				</div>
				<div class="why__we-items">
					<div class="why__we-item why__we-item-1">
						<div class="why__we-item-num">
							1
						</div>
						<div class="why__we-item-text">
							На основе анализа целевой аудитории, рыночной ниши и т.п. создается концепция будущего лендинга. Разработка вероятных сценариев пользовательского поведения дает возможность создать итоговый UI-прототип страницы
						</div>
					</div>
					<div class="why__we-item why__we-item-2">
						<div class="why__we-item-num">
							2
						</div>
						<div class="why__we-item-text">
							Анализ продукта с последующей подготовкой продающих текстов, подбором мотивирующих к покупке фотографий и иных графических элементов
						</div>
					</div>
					<div class="why__we-item why__we-item-3">
						<div class="why__we-item-num">
							3
						</div>
						<div class="why__we-item-text">
							Отрисовка макета на основе имеющегося прототипа. Адаптация готового дизайна под мобильные устройства
						</div>
					</div>
					<div class="why__we-item why__we-item-4">
						<div class="why__we-item-num">
							4
						</div>
						<div class="why__we-item-text">
							Верстка лендинга под любые экранные разрешения и устройства. Тестирование скорости загрузки, работоспособности и правильности отображения в различных браузерах
						</div>
					</div>
					<div class="why__we-item why__we-item-5">
						<div class="why__we-item-num">
							5
						</div>
						<div class="why__we-item-text">
							Добавление и настройка всех необходимых аналитических систем, коллтрекинг и т.п
						</div>
					</div>
					<div class="why__we-item why__we-item-6">
						<div class="why__we-item-num">
							6
						</div>
						<div class="why__we-item-text">
							Окончательная проверка проведенной работы и передача заказчику всех доступов
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>