<?php use App\Models\Accord;?>
<?php $pages = Accord::all(); ?>
<?php $this_page = $page ?>

<section class="accordion">
	<div class="container">
		<div class="accordion__titlee title">
            <?php 
			$pages = Accord::all();
			$name_page = Accord::where('page', $this_page)->first();
			echo $name_page->title;
			?>
		</div>
		<div class="accordion__inner">
            @foreach($pages as $item)
			<div class="accordion__item">
				<div class="accordion__top">
					<div class="accordion__title-wrap">
						<div class="accordion__title">
							{{$item['title_accord']}}
						</div>
						<div class="accordion__item-price">
                            
							<p>{{$item['days']}}</p>
							<p>{{$item['summa']}}</p>
						</div>
						<div class="switch accordion__item-icon"></div>
					</div>
				</div>
				<div class="accordion__text">
					{{$item['content']}}
					<a href="#" class="accordion__btn">{{$item['accord_btn']}}</a>
				</div>
			</div>
            @endforeach
		</div>
	</div>
</section>