

<?php 
    // $pages = Capabilities::all();
    // //$sub_title = CapabilitiesSubtitles::all();
    // $name_page = Capabilities::where('page', $this_page)->first();
    // //$sub_title_page = CapabilitiesSubtitles::where('page', 'home')->get();
?>

<section class="capabilities">
    <div class="container">
        <div class="capabilities__inner">
            <div class="capabilities__title title">
                Мы много<br> чего могем
            </div>
            <div class="capabilities__items-wrap">
                <div class="capabilities__items">
                    <div class="capabilities__item">
                        <a href="/sites" class="capabilities__item-title">
                            создание сайтов
            </a>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Landing page
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Интернет-магазин
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Информационный сайт
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Сайт-визитка
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Работа с сайтами
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Наполнение сайтов
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Почасовые программные работы
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Подключение пикселей и ситем аналитики
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Подключение 1С
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            <span>Настройка рекламных</span>
                            кабинетов
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Яндекс директ и РСЯ
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Google adwords
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Подключение метрик
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            SEO продвижение
                        </div>
                    </div>
                </div>
                <div class="capabilities__items">
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            SMM
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Создание контена
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Настройка реклманого кабинета VK
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Дизайн
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Брендинг и разработка айдентики
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Баннеры для web и печати
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Продуктовый дизайн (упаковка продукции)
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Оформление маркет плейсов
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Оформление социальных сетей,
                                ютуб каналов и т.д
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Почасовые дизайн работы - любые задачи
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Полиграфия
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Дополнительные
                            услуги
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Подбор и общение с блогерами VK, Яндекс Дзен, YouTube, Instagram Посев в группах
                            </div>
                        </div>
                    </div>
                </div>
                <div class="capabilities__items">
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Фотография
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Бьюти фотография
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Модельная сьемка
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Брендбук
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Предметная журнальная сьемка
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Фото для интернет-магазинов и маркет плейсов
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Организация фотосьемки "под ключ"
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Фото корпаративного календаря
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Видеопродакшен
                        </div>
                        <div class="capabilities__item-dscr">
                            <div  class="capabilities__item-dscr-item">
                                Видео для соц сетей
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Видео интервью
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Рекламное видео
                            </div>
                            <div  class="capabilities__item-dscr-item">
                                Анимация
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Маркетинг
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Разработка концепций бренда
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Разработка отрибутов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Анализ конкурентов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Выявление сильных и слабых сторон бренда
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Определение ЦА, анализ ЦА
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Разработка и запуск рекламных компаний
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="capabilities__items-wrap-media">
                <div class="capabilities__items">
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            создание сайтов
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Landing page
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Интернет-магазин
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Информационный сайт
                            </div>
                            <a href="#" class="capabilities__item-dscr-item">
                                Сайт-визитка
                            </a>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Работа с сайтами
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Наполнение сайтов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Почасовые программные работы
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Подключение пикселей и ситем аналитики
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Подключение 1С
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            <span>Настройка рекламных</span>
                            кабинетов
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Яндекс директ и РСЯ
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Google adwords
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Подключение метрик
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            SEO продвижение
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            SMM
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Создание контена
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Настройка реклманого кабинета VK
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Дизайн
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Брендинг и разработка айдентики
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Баннеры для web и печати
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Продуктовый дизайн (упаковка продукции)
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Оформление маркет плейсов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Оформление социальных сетей,
                              divютуб каналов и т.д
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Почасовые дизайн работы - любые задачи
                            </div>
                        </div>
                    </div>
                </div>
                <div class="capabilities__items">
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Полиграфия
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Дополнительные
                            услуги
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Подбор и общение с блогерами VK, Яндекс Дзен, YouTube, Instagram Посев в группах
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Фотография
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Бьюти фотография
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Модельная сьемка
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Брендбук
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Предметная журнальная сьемка
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Фото для интернет-магазинов и маркет плейсов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Организация фотосьемки "под ключ"
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Фото корпаративного календаря
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Видеопродакшен
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Видео для соц сетей
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Видео интервью
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Рекламное видео
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Анимация
                            </div>
                        </div>
                    </div>
                    <div class="capabilities__item">
                        <div class="capabilities__item-title">
                            Маркетинг
                        </div>
                        <div class="capabilities__item-dscr">
                            <div class="capabilities__item-dscr-item">
                                Разработка концепций бренда
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Разработка отрибутов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Анализ конкурентов
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Выявление сильных и слабых сторон бренда
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Определение ЦА, анализ ЦА
                            </div>
                            <div class="capabilities__item-dscr-item">
                                Разработка и запуск рекламных компаний
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>