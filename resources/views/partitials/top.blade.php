<?php use App\Models\Page;?>
<?php $pages = Page::all(); ?>

<?php $this_page = $page ?> 
<section class="top">
    <div class="container">
        <div class="top__inner">
            <div class="top__menu-bar">
                <div class="top__menu-bar-btn"></div>
                <div class="top__menu-bar-logo">
                    <a href="/">
                        <img src="img/logo-bar.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="top__menu-wrapper">
							<a href="/" class="top__menu-logo">
									<img src="img/logo.svg" alt="">
							</a>
							<ul class="menu__list-main">
									<li class="menu__item">
											<a href="/about" class="menu__link">
													О нас
											</a>
									</li>
							</ul>
							<div class="top__menu-inner">
									<div class="top__menu-inner-title">
											Услуги
									</div>
									<nav class="menu">
											<ul class="menu__list">
													<li class="menu__item">
															<a href="/sites?parentId=landingPage" class="menu__link">
																	LANDING PAGE
															</a>
													</li>
													<li class="menu__item">
															<a href="/sites?parentId=onlineStore" class="menu__link">
																	ИНТЕРНЕТ-МАГАЗИН
															</a>
													</li>
													<li class="menu__item">
															<a href="/sites?parentId=websiteCard" class="menu__link">
																	САЙТ-ВИЗИТКА
															</a>
													</li>
													<li class="menu__item">
															<a href="/sites?parentId=corporateWebsite" class="menu__link">
																	КОРПОРАТИВНЫЙ САЙТ
															</a>
													</li>
													<li class="menu__item">
															<a href="/sites?parentId=crmSystem" class="menu__link">
																	CRM СИСТЕМА
															</a>
													</li>
											</ul>
									</nav>
							</div>
							<ul class="menu__list-main">
									<li class="menu__item">
											<a href="/brefe" class="menu__link">
													Заполнить бриф
											</a>
									</li>
							</ul>
						</div>
            <div class="top__text-wrap">
                <div class="top__text">
                    <?php 
                    $name_page = Page::where('page', $this_page)->first();
                    echo $name_page->title;
                    ?>
                </div>
            </div>
            <div class="top__dscr-wrap">
							<a href="#contact" class="top__btn">
								оставить заявку
							</a>
							<a href="#projects" class="top__btn">
								посмотреть примеры работ
							</a>
							<div class="top__dscr-text">
                                <?php echo $name_page->description; ?>
							</div>
            </div>
        </div>
    </div>
</section>