<?php use App\Models\Stuff;?>
<?php $this_page = 'sites' ?>

<section class="staf">
	<div class="container">
		<div class="staf__inner">
				<div class="staf__title title">

					<?php 
					$pages = Stuff::all();
					$name_page = Stuff::where('page', $this_page)->first();
					echo $name_page->title;
					?>
						
				</div>
				<div class="staf__nextTitle">
					<?php
						$name_page = Stuff::where('page', $this_page)->first();
						echo $name_page->description;
					?>
				</div>
		</div>
	</div>
	<div class="staf__items">
		<div class="staf__item">
			<div class="staf__item-border"></div>
			<img src="img/staf-1.jpg" alt="">
			<div class="staf__item-dscr">
					<div class="staf__item-name">
						Мария
					</div>
					<div class="staf__item-job">
						Project-Manager
					</div>
			</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-2.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
							Дина
						</div>
						<div class="staf__item-job">
							UX/UI дизайнер
						</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-3.jpg" alt="">
				<div class="staf__item-dscr">
					<div class="staf__item-name">
							Александр
					</div>
					<div class="staf__item-job">
						Frontend разработчик
					</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-4.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
							Иван
						</div>
						<div class="staf__item-job">
							Full Stack Developer
						</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-5.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
								Иван
						</div>
						<div class="staf__item-job">
								Инжинер-программист
						</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-6.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
								Хамзат
						</div>
						<div class="staf__item-job">
							Видеограф-фотограф
						</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-7.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
							Шамсутдин
						</div>
						<div class="staf__item-job">
							Коммерческий директор
						</div>
				</div>
		</div>
		<div class="staf__item">
				<div class="staf__item-border"></div>
				<img src="img/staf-8.jpg" alt="">
				<div class="staf__item-dscr">
						<div class="staf__item-name">
							Юнус
						</div>
						<div class="staf__item-job">
							Генеральный директор
						</div>
				</div>
		</div>
		<div class="staf__items-btns">
			<div class="staf__items-btn staf__items-prev"></div>
			<div class="staf__items-btn staf__items-next"></div>
		</div>
	</div>
</section>