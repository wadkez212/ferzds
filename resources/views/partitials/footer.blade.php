<footer class="footer">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__item footer__item-1">
                <img src="img/logo.svg" alt="" class="footer__logo">
                <div class="footer__logo-text">
                    Студия WEB разработки,<br>
                    дизайна и маркетинга
                </div>
            </div>
            <div class="footer__item-inner-wrap">
                <div class="footer__item menu__list-footer">
                    <ul class="menu__list">
                        <li class="menu__item">
                            <a href="/sites" class="menu__link">
                                Создание сайтов
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Программные работы
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                SEO-продвижение
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Дизайн
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Полиграфия
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Фото-продакшен
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Разработка рекламмных стратегий и маркетинга
                            </a>
                        </li>
                    </ul>
                    <ul class="menu__list">
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Политика конфиденциальности
                            </a>
                        </li>
                        <li class="menu__item">
                            <a href="#" class="menu__link">
                                Контакты
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="footer__item">
                    <div class="footer__contact-dscr">
                        <p class="footer__title-grey">Телефон:</p>
                        <a href="tel:+79604089497">+7 (960) 408-94-97</a>
                    </div>
                    <div class="footer__contact-dscr">
                        <p class="footer__title-grey">Почта:</p>
                        <a href="mailto:">contact@fds.ru</a>
                    </div>
                    <div class="footer__contact-dscr">
                        <p class="footer__title-grey">Адрес:</p>
                        г. Санкт-Петербруг, ул. Хрустальная 27
                    </div>
                    <div class="footer__social">
                        <p class="footer__title-grey">Соц.сети:</p>
                        <div class="footer__social-items">
                            <a href="#" class="footer__social-item"></a>
                            <a href="#" class="footer__social-item"></a>
                            <a href="#" class="footer__social-item"></a>
                            <a href="#" class="footer__social-item"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="footer__copy">
            OOO “Ферзь”, Copyright 2022
        </p>
    </div>
</footer>