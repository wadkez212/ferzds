<header class="header">
    <div class="container">
        <div class="header__inner">
            <a href="/">
                <img src="img/logo.svg" alt="">
            </a>
            <div class="header__menu-btn"></div>
        </div>
    </div>
    
</header>