<?php use App\Models\Page;?>

<section class="top">
	<div class="container">
		<div class="top__inner">
			<div class="top__menu-bar">
					<div class="top__menu-bar-btn"></div>
					<div class="top__menu-bar-logo">
							<a href="/">
								<img src="img/logo-bar.svg" alt="">
							</a>
					</div>
			</div>
			<div class="top__menu-wrapper">
					<a href="/" class="top__menu-logo">
							<img src="img/logo.svg" alt="">
					</a>
					<ul class="menu__list-main">
							<li class="menu__item">
									<a href="#" class="menu__link">
											Кейсы
									</a>
							</li>
							<li class="menu__item">
									<a href="/about" class="menu__link">
											О нас
									</a>
							</li>
					</ul>
					<div class="top__menu-inner">
							<div class="top__menu-inner-title">
									Услуги
							</div>
							<nav class="menu">
									<ul class="menu__list">
											<li class="menu__item">
													<a href="/sites" class="menu__link">
															Создание сайтов
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Программные работы
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															SEO-продвижение
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Дизайн
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Полиграфия
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Фото-продакшен
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Видео-продакшен
													</a>
											</li>
											<li class="menu__item">
													<a href="#" class="menu__link">
															Разработка рекламных стратегий и проектов
													</a>
											</li>
									</ul>
							</nav>
					</div>
					<ul class="menu__list-main">
							<li class="menu__item">
									<a href="#" class="menu__link">
											Контакты
									</a>
							</li>
							<li class="menu__item">
									<a href="/brefe" class="menu__link">
											Заполнить бриф
									</a>
							</li>
					</ul>
			</div>
			<div class="top__text-wrap">
					<div class="top__text">


						<?php 
							$pages = Page::all();
							$name_page = Page::where('page', 'home')->first();
						    echo $name_page->title;
					
						?>
					</div>
			</div>
			<div class="top__dscr-wrap">
				<a href="/brefe" class="top__btn">
					Заполнить бриф
				</a>
				<div class="top__dscr-text">

					{{$name_page->description;}}
				
					
				</div>
			</div>
		</div>
	</div>
</section>