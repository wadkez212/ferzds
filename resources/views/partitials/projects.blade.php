<?php 
use App\Models\Project;
use App\Models\Tags;
?>
<?php $this_page = $page ?>
<section class="projects" id="projects">
    <div class="container">
        <div class="projects__inner">
            <div class="projects__title title">
				<?php 
				$pages = Project::all();
				$name_page = Project::where('page', $this_page)->first();
				echo $name_page->title;
				?>
            </div>
			<div class="projects__nextTitle nextTitle">
				<?php 
				$pages = Project::all();
				$name_page = Project::where('page', $this_page)->first();
				echo $name_page->description;
				?>
			</div>
        </div>
    </div>
    <div class="projects__items">
		@foreach ($pages as $item)
        <div class="projects__item">
			<img src="<?php echo $name_page->img; ?>" alt="" class="projects__item-img">

					<div class="projects__item-dscr">
							<div class="projects__item-title">
								<?php 
								$pages = Project::all();
								$name_page = Project::where('page', $this_page)->first();
								echo $name_page->name_project;
								?>
							</div>
							<div class="projects__item-tegs">
									{{--  вместо связи один ко многим --}}
									<?php 
									$name_page = Project::where('page', $this_page)->first();
									$string = $name_page->tags;
								
									$tags = json_decode($string);
							
									foreach ($tags as $key) {
										$tag = Tags::where('id', (int)$key)->first();
										echo "<span>";
											echo $tag->title; 
										echo "</span>";
									}
									
									?>
							</div>
					</div>
					<div class="projects__item-show">
							<div class="projects__item-text">
								<?php 
									$pages = Project::all();
									$name_page = Project::where('page', $this_page)->first();
									echo $name_page->description_hover;
								?>
							</div>
							<a href="<?php echo $name_page->link_project; ?>" target="_blank" class="projects__item-btn">
								Подробнее о проекте
							</a>
					</div>
        </div>
		@endforeach
    </div>
		<div class="projects__dscrText">
			<?php 
			$pages = Project::all();
			$name_page = Project::where('page', $this_page)->first();
			echo $name_page->dscrText;
			?>
		</div>
    <a href="#contact" class="projects__btn">
		<?php 
		$pages = Project::all();
		$name_page = Project::where('page', $this_page)->first();
		echo $name_page->button_text;
		?>
    </a>
</section>