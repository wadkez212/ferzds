<?php use App\Models\Stuff;?>
<?php $this_page = $page ?>

<section class="dscr">
	<div class="container">
		<div class="dscr__inner">
			<div class="dscr__content">
				<div class="dscr__title title">
                    <?php 
                    $pages = Stuff::all();
                    $name_page = Stuff::where('page', $this_page)->first();
                    echo $name_page->title;
                    ?>
				</div>
				<div class="dscr__text">
                    <?php echo $name_page->description; ?>
				</div>
			</div>
			<div class="dscr__image"></div>
		</div>
	</div>
</section>