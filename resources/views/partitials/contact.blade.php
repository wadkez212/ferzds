<section class="contact">
    <div class="container">
        <div class="contact__title title">
            <span>Свяжитесь</span> с нами
        </div>
        <div class="contact__inner">
            <div class="contact__dscr">
                <div class="contact__text">
                    Предложим Вам рабочие идеи <br>и маркетинговые фишки
                </div>
                <div class="contact__items">
                    <div class="contact__item">
                        1. Наш Маркетолог свяжется с Вами, уточнит цели и особенности проекта<br>
                    </div>
                    <div class="contact__item">
                        2. Соберет крутые решения и функционал для Вашей сферы —
                        <span>бесплатно</span>
                    </div>
                    <div class="contact__item">
                        3. Подготовит 3-4 варианта действий и расскажет рабочие маркетинговые ходы
                    </div>
                </div>
                <div class="contact__text">
                    P.S. У вас есть ТЗ? Присылайте нам — <br>
                    Соберем идеи и рассчитаем<br> в 3х вариантах!
                </div>
            </div>
            <div class="contact__form">
                <form action="/mail" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @method('POST')
                    <div class="input__wrapper">
                        <input type="text" name="name" placeholder="Имя *">
                    </div>
                    <div class="input__wrapper">
                        <input type="tel" name="phone" placeholder="Телефон *">
                    </div>
                    <div class="input__wrapper">
                        <input type="email" name="email" placeholder="Email *">
                    </div>
                    <div class="input__wrapper">
                        <label for="file" id="lebelFile">Приложить файл (до 10 Мб)</label>
                        <input type="file" name="file[]" id="file">
                    </div>
                    <button type="submit">отправить</button>
                    <p>
                        Нажимая на кнопку "Отправить", вы принимаете <a href="#">политику конфиденциальности</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</section>