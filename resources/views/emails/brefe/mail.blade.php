<div>
    <h2>Новый заполненный бриф</h2>
    <h3>Вид услуги: </h3>
    <p><?php echo  $data['category'] ;?></p>
    <h3>Имя:</h3>
    <p><?php echo  $data['name'] ;?></p>
    <h3>Телефон: </h3>
    <p><?php echo  $data['phone'] ;?></p>
    <h3>Имя компании: </h3>
    <p><?php echo  $data['nameCompany'] ;?></p>
    <h3>Описание основных продутов/услуг: </h3>
    <p><?php echo  $data['descriptionOfServices'] ;?></p>
    <h3>Конкуренты: </h3>
    <p><?php echo  $data['competitors'] ;?></p>
    <h3>Целевая аудитория: </h3>
    <p><?php echo  $data['audience'] ;?></p>
    <h3>Что вас привело создать новый сайт: </h3>
    <p><?php echo  $data['target'] ;?></p>
    <h3>Предварительная структура сайта: </h3>
    <p><?php echo  $data['structure'] ;?></p>
    <h3>Какие материалы у вас есть?: </h3>
    <p><?php echo  $data['materials'] ;?></p>
    <h3>Сроки сдачи проекта: </h3>
    <p><?php echo  $data['deadline'] ;?></p>
    <h3>Предварительный бюджет: </h3>
    <p><?php echo  $data['budget'] ;?></p>
    <h3>Требования к дизайну: </h3>
    <p><?php echo  $data['designRequirements'] ;?></p>
    <h3>Понравившиеся сайты: </h3>
    <p><?php echo  $data['websiteAddressesLike'] ;?></p>
    <h3>Дополнительная информация: </h3>
    <p><?php echo  $data['info'] ;?></p>
</div>
