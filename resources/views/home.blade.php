
<!DOCTYPE html>
<html lang="ru">

    @include('/partitials/head')

<body>

	<?php $this_page = 'home' ?>

    @include('/partitials/header')

	@include('/partitials/home_top_part')

	@include('/partitials/staf_team_carousel')

	@include('/partitials/why_we')

	@include('/partitials/projects')

	@include('/partitials/contact')

	@include('/partitials/capabilities')

	@include('/partitials/footer')


<!-- <section class="video">
	<div class="container__video">
		<div class="video__inner">
			<video autoplay="autoplay" poster="video/video-preview.jpg">
				<source src="video/" type="video/mp4">
			</video>
		</div>
	</div>
</section> -->




<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
<script src="js/polyfills.js"></script>
<script src="js/main.js" type="module"></script>
</body>
</html>
