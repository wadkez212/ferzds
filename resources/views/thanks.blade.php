<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FERZDS</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/brefe.css">
</head>
<body>

	<style>
		img{
			width: 200px;
			margin-bottom: 20px;
			display: block;
		}
		h1{
			font-weight: 400;
			font-size: 23px;
			color: #fff;
			margin-bottom: 10px;
		}
		p{
			font-size: 16px;
			color: #fff;
		}
	</style>

	<img src="../img/icons/thanks.svg" alt="">
	<h1>Мы получили заполненную Вами форму</h1>
	<p>В течении 5 минут с Вами свяжется наш менеджер для уточнения всех деталей</p>

	<script>
		setTimeout(() => {
			window.location.href = '/';
		}, 4000);
	</script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
	<script src="../js/polyfills.js"></script>
	<script src="../js/main.js" type="module"></script>
</body>
</html>