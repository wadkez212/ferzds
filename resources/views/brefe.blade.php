<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FERZDS</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/brefe.css">
</head>
<body>
	<a href="/" class="logo">
		<img src="../img/logo.svg" alt="">
	</a>
	<div class="title">
		Разработка и <br>продвижение сайтов
	</div>

	<form action="/mail/brefe" method="post" class="brefe__form">
        {{ csrf_field() }}
        @method('POST')
		<div class="brefe_slides">
			<div class="brefe_1 brefe_slide active">
				<div class="input__wrapper">
					<div class="select__wrap">
						<input type="hidden" name="category" id="category">
						<div class="select__value">Выберите категорию</div>
						<div class="select__option-wrap">
							<div class="select__option-wrap-item">
								Landing page
							</div>
							<div class="select__option-wrap-item">
								Интернет-магазин
							</div>
							<div class="select__option-wrap-item">
								Сайт-визитка
							</div>
							<div class="select__option-wrap-item">
								Информационный сайт
							</div>
							<div class="select__option-wrap-item">
								Только дизайн сайта
							</div>
							<div class="select__option-wrap-item">
								CRM система
							</div>
						</div>
					</div>
				</div>
				<div class="input__wrapper">
					<input type="text" name="name" placeholder="Имя *">
				</div>
				<div class="input__wrapper">
					<input type="phone" name="phone" placeholder="Телефон *">
				</div>
				<div class="input__wrapper">
					<input type="email" name="email" placeholder="Email *">
				</div>
				<div class="input__wrapper input__wrapper-btn">
					<span class="button__line button__line--top"></span>
					<span class="button__line button__line--right"></span>
					<span class="button__line button__line--bottom"></span>
					<span class="button__line button__line--left"></span>
					Далее
				</div>
			</div>
			<div class="brefe_2 brefe_slide">
				<p>Информация о компании</p>
				<div>
					<div class="input__wrapper">
						<input type="text" name="nameCompany" placeholder="Полное название компании/организации">
					</div>
					<div class="input__wrapper">
						<textarea name="descriptionOfServices" placeholder="Описание основных продуктов/услуг"></textarea>
					</div>
				</div>
				<div class="input__wrapper input__wrapper-btn">
					<span class="button__line button__line--top"></span>
					<span class="button__line button__line--right"></span>
					<span class="button__line button__line--bottom"></span>
					<span class="button__line button__line--left"></span>
					Далее
				</div>
			</div>
			<div class="brefe_3 brefe_slide">
				<p>Конкуренты</p>
				<div>
					<div class="input__wrapper">
						<textarea class="textarea-big" name="competitors" placeholder="Укажите прямых конкурентов в Вашем ценном сегменте. По возможности охарактеризуйте их сильные и слабые стороны. Укажите адреса сайтов."></textarea>
					</div>
				</div>
				<div class="input__wrapper input__wrapper-btn">
					<span class="button__line button__line--top"></span>
					<span class="button__line button__line--right"></span>
					<span class="button__line button__line--bottom"></span>
					<span class="button__line button__line--left"></span>
					Далее
				</div>
			</div>
			<div class="brefe_4 brefe_slide">
				<p>Целевая аудитория</p>
				<div>
					<div class="input__wrapper">
						<textarea class="textarea-big" name="audience" placeholder="Кто принимает решение о покупке продукта или услуги? Его социально-демографические характеристики (пол, возраст, доход, образование, стиль жизни)"></textarea>
					</div>
				</div>
				<div class="input__wrapper input__wrapper-btn">
					<span class="button__line button__line--top"></span>
					<span class="button__line button__line--right"></span>
					<span class="button__line button__line--bottom"></span>
					<span class="button__line button__line--left"></span>
					Далее
				</div>
			</div>
			<div class="brefe_5 brefe_slide">
				<p>Информация о интенет-проекте</p>
				<div>
					<div class="input__wrapper">
						<textarea name="target" placeholder="Что привело Вас к решению создать новый сайт (изменить существующий)?"></textarea>
					</div>
					<div class="input__wrapper">
						<textarea name="structure" placeholder="Напишите предварительную структуру сайта: основные разделы, подразделы. Кратко опишите их функциональное назначение и дайте характеристику содержания каждого из разделов."></textarea>
					</div>
					<div class="input__wrapper">
						<textarea name="materials" placeholder="Какие материалы у Вас у есть? (подсказка: Логотип, знак, фирменный цвет, фирменный шрифт, фотографии, материалы, используемые при разработке другой рекламной продукции и т. д.?)"></textarea>
					</div>
					<div class="input__wrapper">
						<input type="text" name="deadline" placeholder="Желаемые сроки разработки сайта">
					</div>
					<div class="input__wrapper">
						<input type="text" name="budget" placeholder="Бюджет проекта (от и до)">
					</div>
					<div class="input__wrapper input__wrapper-btn">
						<span class="button__line button__line--top"></span>
						<span class="button__line button__line--right"></span>
						<span class="button__line button__line--bottom"></span>
						<span class="button__line button__line--left"></span>
						Далее
					</div>
				</div>
			</div>
			<div class="brefe_6 brefe_slide">
				<p>ОСНОВНЫЕ ТРЕБОВАНИЯ <br> И ПОЖЕЛАНИЯ К ДИЗАЙНУ</p>
				<div>
					<div class="input__wrapper">
						<textarea name="designRequirements" placeholder="Напишите требования к дизайну, которые обязательны для исполнения. Пожелания к дизайну сайта"></textarea>
					</div>
					<div class="input__wrapper">
						<textarea name="websiteAddressesLike" placeholder="Напишите адреса нескольких сайтов, которые Вам нравятся. Что именно Вам нравится в этих сайтах (стильный дизайн, удобная навигация и т. п.)?"></textarea>
					</div>
					<div class="input__wrapper input__wrapper-btn">
						<span class="button__line button__line--top"></span>
						<span class="button__line button__line--right"></span>
						<span class="button__line button__line--bottom"></span>
						<span class="button__line button__line--left"></span>
						Далее
					</div>
				</div>
			</div>
			<div class="brefe_7 brefe_slide">
				<p>ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</p>
				<div>
					<div class="input__wrapper">
						<textarea class="textarea-big" name="info" placeholder="Любая полезная в работе над проектом информация"></textarea>
					</div>
				</div>
				<button type="submit">Отправить</button>
			</div>
		</div>
	</form>



	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
	<script src="../js/polyfills.js"></script>
	<script src="../js/main.js" type="module"></script>
</body>
</html>

