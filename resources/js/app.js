require('./bootstrap');

export function scrollToggle() {
    window.disableScroll = function (param) {
        const widthScroll = window.innerWidth - document.body.offsetWidth;

        document.body.dbScrollY = window.scrollY;

        const body = document.body.style.cssText = `
			position: fixed;
			top: ${-window.scrollY}px;
			left: 0;
			bottom: 0;
			width: 100%;
			overflow: hidden;
			padding-right: ${widthScroll}px;
		`;
    };

    window.enableScroll = function (param) {
        document.body.style.cssText = `position: static;`;
        window.scroll({ top: document.body.dbScrollY });
    };
}
