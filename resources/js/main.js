function scrollToggle() {
	window.disableScroll = function (param) {
		const widthScroll = window.innerWidth - document.body.offsetWidth;

		document.body.dbScrollY = window.scrollY;

		const body = document.body.style.cssText = `
			position: fixed;
			top: ${-window.scrollY}px;
			left: 0;
			bottom: 0;
			width: 100%;
			overflow: hidden;
			padding-right: ${widthScroll}px;
		`;
	};

	window.enableScroll = function (param) {
		document.body.style.cssText = `position: static;`;
		window.scroll({ top: document.body.dbScrollY });
	};
}
scrollToggle();

function toggleMenu() {
	const menuBtn = document.querySelector('.top__menu-bar-btn');
	const menuWrap = document.querySelector('.top__menu-wrapper');

	if (menuBtn) {
		menuBtn.addEventListener('click', function () {
			menuWrap.classList.toggle('active');
			this.classList.toggle('active');
		});
		document.body.addEventListener('click', function (event) {
			if (window.innerWidth > 600) {
				if (menuWrap.matches('.active')) {
					if (!event.target.closest('.top__menu-wrapper') && !event.target.closest('.top__menu-bar-btn')) {
						menuWrap.classList.remove('active');
						menuBtn.classList.remove('active');
					}
				}
			}
		});
	}
}
toggleMenu();

function toggleMenuMedia() {
	const btn = document.querySelector('.header__menu-btn');
	const menu = document.querySelector('.top__menu-wrapper');

	if (menu) {
		btn.addEventListener('click', function () {
			this.classList.toggle('active');
			menu.classList.toggle('activeMob');
		});
	}
}
toggleMenuMedia();

function scrollWhyWe() {
	const item1 = document.querySelector('.why__we-item-1');
	const item2 = document.querySelector('.why__we-item-2');
	const item3 = document.querySelector('.why__we-item-3');
	const item4 = document.querySelector('.why__we-item-4');

	const whyWeItems1 = document.querySelector('.why__we-items-1');

	if (item1 && item2 && item3 && item4 && whyWeItems1) {
		window.addEventListener('scroll', function () {
		if (item1.getBoundingClientRect().top <= 230) {
			item1.classList.add('active');
		} else {
			item1.classList.remove('active');
		}
		if (item1.getBoundingClientRect().top < 90) {
			item1.classList.remove('active');
		}

		if (item2.getBoundingClientRect().top <= 230) {
			item2.classList.add('active');
		} else {
			item2.classList.remove('active');
		}
		if (item2.getBoundingClientRect().top < 50) {
			item2.classList.remove('active');
		}

		if (item3.getBoundingClientRect().top <= 230) {
			item3.classList.add('active');
		} else {
			item3.classList.remove('active');
		}
		if (item3.getBoundingClientRect().top < 90) {
			item3.classList.remove('active');
		}

		if (item4.getBoundingClientRect().top <= 230) {
			item4.classList.add('active');
		} else {
			item4.classList.remove('active');
		}
		if (item4.getBoundingClientRect().top < 50) {
			item4.classList.remove('active');
		}

		if (item5.getBoundingClientRect().top <= 230) {
			item5.classList.add('active');
		} else {
			item5.classList.remove('active');
		}
	});
	}
}
scrollWhyWe();

function scrollWhyWe2() {
	const item1 = document.querySelector('.why__we-item-1');
	const item2 = document.querySelector('.why__we-item-2');
	const item3 = document.querySelector('.why__we-item-3');
	const item4 = document.querySelector('.why__we-item-4');
	const item5 = document.querySelector('.why__we-item-5');
	const item6 = document.querySelector('.why__we-item-6');


	const whyWeTitle = document.querySelector('.why__we-title');

	if (item1 && item2 && item3 && item4 && item5 && item6) {
		window.addEventListener('scroll', function () {
			if (item1.getBoundingClientRect().top <= 230) {
				item1.classList.add('active');
				whyWeTitle.innerHTML = "Прототипирование";
			} else {
				item1.classList.remove('active');
			}
			if (item1.getBoundingClientRect().top < 90) {
				item1.classList.remove('active');
			}

			if (item2.getBoundingClientRect().top <= 230) {
				item2.classList.add('active');
				whyWeTitle.innerHTML = "Написание <br>и подготовка <br>наполнения";
			} else {
				item2.classList.remove('active');
			}
			if (item2.getBoundingClientRect().top < 50) {
				item2.classList.remove('active');
			}

			if (item3.getBoundingClientRect().top <= 230) {
				item3.classList.add('active');
				whyWeTitle.innerHTML = "Разработка UI/UX <br>дизайна";
			} else {
				item3.classList.remove('active');
			}
			if (item3.getBoundingClientRect().top < 90) {
				item3.classList.remove('active');
			}

			if (item4.getBoundingClientRect().top <= 230) {
				item4.classList.add('active');
				whyWeTitle.innerHTML = "Адаптивная верстка, <br>тестирование";
			} else {
				item4.classList.remove('active');
			}
			if (item4.getBoundingClientRect().top < 50) {
				item4.classList.remove('active');
			}

			if (item5.getBoundingClientRect().top <= 230) {
				item5.classList.add('active');
				whyWeTitle.innerHTML = "Подключение <br>внешних решений";
			} else {
				item5.classList.remove('active');
			}
			if (item5.getBoundingClientRect().top < 90) {
				item5.classList.remove('active');
			}

			if (item6.getBoundingClientRect().top <= 230) {
				item6.classList.add('active');
				whyWeTitle.innerHTML = "Финальные правки <br>и старт лендинга";
			} else {
				item6.classList.remove('active');
			}
			if (item6.getBoundingClientRect().top < 50) {
				item6.classList.remove('active');
			}
		});
	}
}
scrollWhyWe2();


let count = 2;
let size = 73;

let ww = jQuery(window).width();
if (ww <= 1112) count = 1;

let ww2 = jQuery(window).width();
if (ww2 <= 1112) size = 20;

let swiper = new Swiper(".reviews__slider", {
	slidesPerView: count,
	spaceBetween: size,
	pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},
	navigation: {
		nextEl: ".reviews__slider-arrows-next",
		prevEl: ".reviews__slider-arrows-prev",
	},
});

function accordionToggle() {
	const accordionTitle = document.querySelectorAll('.accordion__top');
	const accordionBody = document.querySelectorAll('.accordion__text');
   
	if (accordionBody) {
	 accordionTitle.forEach(element => {
	  element.addEventListener('click', function (event) {
	  const accordionBodyOpened = element.nextElementSibling;
		const icon = element.querySelector('.accordion__item-icon');
   
		if (element.classList.contains('opened')) {
			element.classList.remove('opened');
			accordionBodyOpened.classList.remove('opened');
			accordionBodyOpened.style.maxHeight = null;
			icon.classList.remove('active');
	   }
	   else {
			accordionTitle.forEach(element => {
		 	element.classList.remove('opened');
		});
		accordionBody.forEach(element => {
			element.classList.remove('opened');
			element.style.maxHeight = null;
		});
		element.classList.add('opened');
		accordionBodyOpened.classList.add('opened');
		accordionBodyOpened.style.maxHeight = accordionBodyOpened.scrollHeight + "px";
		icon.classList.add('active');
	   }
	  });
	 });
	}
}
accordionToggle();

function brefe() {
	function brefeSelect() {
		const selectValue = document.querySelector('.select__value');
		const selectOptionWrap = document.querySelector('.select__option-wrap');
		const categoryInput = document.querySelector('#category');

		if (selectValue) {
			selectValue.addEventListener('click', function (event) {
				this.nextElementSibling.classList.toggle('active');
				this.classList.toggle('active');
			});
			selectOptionWrap.addEventListener('click', function () {
				if (event.target.matches('.select__option-wrap-item')) {
					selectValue.textContent = "";
					selectValue.textContent = event.target.textContent;
					categoryInput.value = event.target.textContent;

					selectOptionWrap.classList.remove('active');
					selectValue.classList.remove('active');
				}
			});
		}
	}
	brefeSelect();

	function brefeSlide() {
		const inputWrapperBtn = document.querySelectorAll('.input__wrapper-btn');
		const form = document.querySelector('.brefe__form');

		if (form) {
			inputWrapperBtn.forEach(element => {
				element.addEventListener('click', function () {
					let wrapThisSlide = element.closest('.brefe_slide');

					wrapThisSlide.classList.remove('active');
					setTimeout(() => {
						wrapThisSlide.classList.add('hide');
					}, 100);
					form.scrollTo(0, 0);
					wrapThisSlide.nextElementSibling.classList.add('active');
				});
			});
		}
	}
	brefeSlide();
}
brefe();

function staffScroll() {
	const staff = document.querySelector('.staf__items');

	const prev = document.querySelector('.staf__items-prev');
	const next = document.querySelector('.staf__items-next');

	if (staff) {
		next.addEventListener('click', function () {
			staff.scrollTo({
				top: 0,
				left: 2000,
				behavior: 'smooth'
			});
		});
		prev.addEventListener('click', function () {
			staff.scrollTo({
				top: 0,
				left: 0,
				behavior: 'smooth'
			});
		});
	}
}
staffScroll();

function clientsScroll() {
	const clients = document.querySelector('.clients__items');

	const prev = document.querySelector('.clients__items-prev');
	const next = document.querySelector('.clients__items-next');

	if (clients) {
		next.addEventListener('click', function () {
			clients.scrollTo({
				top: 0,
				left: 2000,
				behavior: 'smooth'
			});
		});
		prev.addEventListener('click', function () {
			clients.scrollTo({
				top: 0,
				left: 0,
				behavior: 'smooth'
			});
		});
	}
}
clientsScroll();

