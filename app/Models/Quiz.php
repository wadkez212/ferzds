<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model // таблица Quiz в бд
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'category',
        'descriptionOfServices',
        'name',    
        'email',
        'phone',
        'nameCompany',
        'competitors',
        'audience',
        'target',
        'structure',
        'materials',
        'deadline',
        'budget',
        'designRequirements',
        'websiteAddressesLike',
        'info'

    ];

    //использовали в контроллере функцию добавления в бд, которая уже есть в классе extends
    // public static function create(Request $data) {
    //     $quiz = new Quiz;
 
    //     $quiz['name'] = $data->name;
    //     $quiz['phone'] = $data->phone;
    //     $quiz['category'] = $data->category;
 
    //     $quiz->save();

    // }



     
}
