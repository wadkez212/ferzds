<?php

namespace App\Http\Controllers;

use App\Mail\BrefeMail;
use App\Mail\InfoMail;

use App\Models\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function index(Request $request){
        $data = $request->request->all();
        Mail::to('bitrimmer@mail.ru')->send(new InfoMail($data));
        return view('thanks');
    }


    public function brefe(Request $request){
        $data = $request->request->all();
        Mail::to('bitrimmer@mail.ru')->send(new BrefeMail($data));
        Mail::to($data['email'])->send(new BrefeMail($data));
        Quiz::create($data);
        return view('thanks');
    }
}
