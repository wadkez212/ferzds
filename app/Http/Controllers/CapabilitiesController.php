<?php

namespace App\Http\Controllers;

use App\Models\Capabilities;
use Illuminate\Http\Request;

class CapabilitiesController extends Controller
{
    public function allDataCapabilities(){
        $capabilities = Capabilities::all();
        return view('home', ['capabilities'=>$capabilities]); // в первый параметр pages поулчаем все значениея из бд pages
    }
}
