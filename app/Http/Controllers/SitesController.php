<?php

namespace App\Http\Controllers;

use App\Models\Quiz;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


//НЕНУЖНЫЙ КОНТРОЛЛЕР
class SitesController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
		$posts = Quiz::find(1);  //  Найти объект QUIZ и в разделе attributes мы увидим наши сущности, которые мы добавили в таблицу
		echo $posts->category; // получил атрибут category_quiz из  сущности Quiz

		$posts = Quiz::all();
		foreach($posts as $post) {
			dump($post->name);    // получил все атрибуты name из сущности Quiz
		}

		$posts = Quiz::where('email', 'mail_second@mail.ru')->get(); //извлеч посты с определенным email
		foreach($posts as $post) {
			dump($post->email);    // получил все атрибуты name из сущности Quiz
		}
	}
}
