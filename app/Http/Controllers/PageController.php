<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;

class PageController extends Controller
{
    public function allDataStart(){
        $pages = Page::all();
        return view('home', ['start'=>$pages]); // в первый параметр pages поулчаем все значениея из бд pages
    }
}
