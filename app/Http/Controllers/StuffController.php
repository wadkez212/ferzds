<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Stuff;

class StuffController extends Controller
{
    public function allDataStuff(){
        $stuff = Stuff::all();
        return view('home', ['stuffs'=>$stuff]); // в первый параметр pages поулчаем все значениея из бд pages
    }
}
