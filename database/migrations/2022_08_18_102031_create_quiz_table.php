<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('category')->nullable();
            $table->string('descriptionOfServices')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('nameCompany')->nullable();
            $table->string('competitors')->nullable();
            $table->string('audience')->nullable();
            $table->string('target')->nullable();
            $table->string('structure')->nullable();
            $table->string('materials')->nullable();
            $table->string('deadline')->nullable();
            $table->string('budget')->nullable();
            $table->string('designRequirements')->nullable();
            $table->string('websiteAddressesLike')->nullable();
            $table->string('info')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quiz');

    }
}
