<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accords', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('title_accord')->nullable();
            $table->string('days')->nullable();
            $table->string('summa')->nullable();
            $table->string('content')->nullable();
            $table->string('accord_btn')->nullable();
            $table->string('page')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accords');
    }
}
