<?php

use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\SitesController;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
//Route::get('/home',  [PageController::class, 'allDataStart']);

Route::get('/', function () {
    return view('home', ['page' => 'home']); // в первый параметр page поулчаем все значениея из бд home (pageController так же описано)
});                                         // можно передать несколько параметров (объект product и тд)
Route::get('/about', function () {
    return view('about');
});

Route::get('/thanks', function () {
    return view('thanks');
});
Route::get('/mantis', function () {
    redirect('/mantis/index.php');
});
Route::get('/brefe', function () {
    return view('brefe');
});
Route::get('/sites', function () {
    return view('sites',['page' => 'sites']); // в первый параметр page поулчаем все значениея из бд sites
});                                            // можно передать несколько параметров (объект product и тд)



Route::get('/landing', function () {
    return view('sites',['page' => 'landing']);
});

Route::get('/shop', function () {
    return view('sites',['page' => 'shop']);
});

Route::get('/crm', function () {
    return view('sites',['page' => 'shop']);
});
Route::post('/mail',  [MailController::class, 'index']);