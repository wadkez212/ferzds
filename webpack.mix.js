const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);
mix.copy('resources/img/', 'public/img');
mix.copy('resources/video/', 'public/video');
mix.sass('resources/css/style.scss', 'public/css/style.css');
mix.sass('resources/css/sites.scss', 'public/css/sites.css');
mix.sass('resources/css/about_us.scss', 'public/css/about_us.css');
mix.sass('resources/css/brefe.scss', 'public/css/brefe.css');
mix.js('resources/js/main.js', 'public/js/main.js');
mix.js('resources/js/polyfills.js', 'public/js/polyfills.js');
mix.js('resources/js/slick-slider.js', 'public/js/slick-slider.js');
